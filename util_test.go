package util

import (
	"fmt"
	"testing"
)

func TestDjq(t *testing.T){
	a,_:=TimeParse("2021-11-25 21:25:00")
	fmt.Println(a)
}

func TestRandomString(t *testing.T){
	a:= StringRandom(16)
	if len(a)!=16{
		t.Errorf("a=%v,a_len=%v,expect_len=%v",a,len(a),16)
	}
}

type CheckIdCardTable struct{
	a1 string
	e1 bool
}

func TestCheckIdCard(t *testing.T){
	table :=[]CheckIdCardTable{
		{"440203199108202417",true},
		{"440203199108202411",false},
	}
	for _,item := range table{
		r1:= CheckIdCard(item.a1)
		if r1!=item.e1{
			t.Errorf("item=%v,return=%v",item,[]interface{}{r1})
		}
	}
}

func TestCheckUnicomPhone(t *testing.T){
	table :=[]CheckIdCardTable{
		{"19925012015",false},
		{"1258889652",false},
		{"18620263521",true},
	}
	for _,item := range table{
		r1:= CheckUnicomPhone(item.a1)
		if r1!=item.e1{
			t.Errorf("item=%v,return=%v",item,[]interface{}{r1})
		}
	}
}

func TestExcelCharToNum(t *testing.T){
	table :=[]struct{
		a1 string
		e1 int
	}{
		{"",0},
		{"A",1},
		{"AA",27},
	}
	for _,item := range table{
		r1:= ExcelCharToNum(item.a1)
		if r1!=item.e1{
			t.Errorf("item=%v,return=%v",item,[]interface{}{r1})
		}
	}
}

func TestExcelNumToChar(t *testing.T){
	table :=[]struct{
		a1 int
		e1 string
	}{
		{0,""},
		{1,"A"},
		{27,"AA"},
	}
	for _,item := range table{
		r1:= ExcelNumToChar(item.a1)
		if r1!=item.e1{
			t.Errorf("item=%v,return=%v",item,[]interface{}{r1})
		}
	}
}