package util

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

func Md5(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	cipherStr := h.Sum(nil)
	return hex.EncodeToString(cipherStr)
}

func Sha256(str string) string {
	h := sha256.New()
	h.Write([]byte(str))
	cipherStr := h.Sum(nil)
	return hex.EncodeToString(cipherStr)
}

func StringRandom(l int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < l; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

//中文字符串截取
func MbStringSub(str string, begin, length int) string {
	rs := []rune(str)
	lth := len(rs)
	if begin < 0 {
		begin = 0
	}
	if begin >= lth {
		begin = lth
	}
	end := begin + length

	if end > lth {
		end = lth
	}
	return string(rs[begin:end])
}

func StringToFloat(str string) float64 {
	value, _ := strconv.ParseFloat(str, 64)
	return value
}

func StringToInt(str string) int {
	return int(StringToFloat(str))
}

//字符串数组去重
func StringArrayUnique(arr []string) []string {
	arrMap := make(map[string]string)
	for _, item := range arr {
		arrMap[item] = item
	}
	arr2 := make([]string, 0)
	for _, item2 := range arrMap {
		arr2 = append(arr2, item2)
	}
	return arr2
}

//
func TimeFormat(target time.Time, layout string) string {
	if target.Unix() > 0 {
		return target.Format(layout)
	}
	return ""
}

//设置北京时间
func TimeParse(str string) (time.Time, error) {
	layout := "2006-01-02 15:04:05"
	value,err:= time.Parse(layout[:len(str)], str)
	if err!=nil{
		return time.Time{},err
	}
	return value.Local().Add(8*time.Hour),nil
}

//数字数组去重
func IntArrayUnique(arr []int) []int {
	arrMap := make(map[int]int)
	for _, item := range arr {
		arrMap[item] = item
	}
	arr2 := make([]int, 0)
	for _, item2 := range arrMap {
		arr2 = append(arr2, item2)
	}
	return arr2
}

func IntJoin(arr []int) string {
	value := ","
	for _, item := range arr {
		item2 := strconv.Itoa(item)
		value += item2 + ","
	}
	return value
}

func IntSplit(str string) []int {
	arr := make([]int, 0)
	arr2 := strings.Split(str, ",")
	for _, item := range arr2 {
		if item2 := StringToInt(item); item2 != 0 {
			arr = append(arr, item2)
		}
	}
	return arr
}

//在数组中
func IntInArray(target int, arr []int) bool {
	for _, item := range arr {
		if target == item {
			return true
		}
	}
	return false
}

//两数组求差集
func IntArrayDiff(target []int, deduct []int) []int {
	diff := make([]int, 0)
	for _, targetItem := range target {
		flag := false
		for _, deductItem := range deduct {
			if targetItem == deductItem {
				flag = true
				break
			}
		}
		if !flag {
			diff = append(diff, targetItem)
		}
	}
	return diff
}

//保留x位小数
func FloatRound(value float64, num int) float64 {
	return StringToFloat(fmt.Sprintf("%."+strconv.Itoa(num)+"f", value))
}

//判断文件夹是否存在
func PathExist(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

// 确保文件路径
func EnsurePath(path string) error {
	_, err := os.Stat(path)
	if err == nil {
		//文件夹已存在
		return nil
	}
	if os.IsNotExist(err) {
		err2 := os.MkdirAll(path, os.ModePerm)
		if err2 != nil {
			return err2
		} else {
			return nil
		}
	}
	return err
}
